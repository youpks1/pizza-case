<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomePageController::class)
    ->name('home');

Route::get('admin', AdminPageController::class)
    ->middleware('auth')
    ->name('admin');

Route::post('order/store', [OrderController::class, 'store'])
    ->name('order.store');

Route::post('admin/order/status/update/{order}', [OrderController::class, 'statusUpdate'])
    ->middleware('auth')
    ->name('order.update.status');

require __DIR__.'/auth.php';
