<?php use App\Enums\NotifyType; ?>
<x-guest-layout>
    <x-card>
        <div class="flex justify-between h-full">
            <div class="flex">
                <div class="hidden sm:-my-px sm:flex">
                    @auth
                        <x-nav-link :href="route('admin')" :active="request()->routeIs('admin')">
                            {{ __('Admin dashboard') }}
                        </x-nav-link>
                    @elseguest
                        <x-nav-link class="pb-2 mr-4" :href="route('auth.login')" :active="request()->routeIs('auth.login')">
                            {{ __('Login') }}
                        </x-nav-link>
                        <x-nav-link class="pb-2" :href="route('auth.register')" :active="request()->routeIs('auth.register')">
                            {{ __('Register') }}
                        </x-nav-link>
                    @endauth
                </div>
            </div>
        </div>
        <hr class="mb-4">
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('order.store') }}">
            @csrf

            <div>
                <x-label class="ml-4 mb-2 text-cyan-800 font-bold text-md" for="order[branch_id]" :value="__('Branch')" />

                <select name="order[branch_id]" id="order[branch_id]"
                        class="w-full border-[1.5px] border-cyan-800 rounded-lg duration-75 duration-75
                        active:border-cyan-800 focus:border-cyan-800 focus:outline-offset-0 focus:shadow-none">
                    @foreach($branches as $branch)
                        <option value="{{ $branch->id }}">{{ $branch->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-4">
                <x-label class="ml-4 mb-2 text-cyan-800 font-bold text-md" for="pizza[bottom]" :value="__('Bottom')" />

                <select name="pizza[bottom]" id="pizza[bottom]"
                        class="w-full border-[1.5px] border-cyan-800 rounded-lg duration-75 duration-75
                        active:border-cyan-800 focus:border-cyan-800 focus:outline-offset-0 focus:shadow-none">
                    @foreach($bottoms as $bottom)
                        <option value="{{ $bottom->id }}">{{ $bottom->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-4">
                <x-label class="ml-4 mb-2 text-cyan-800 font-bold text-md" for="pizza[topping]" :value="__('Topping')" />

                <select name="pizza[topping]" id="pizza[topping]"
                        class="w-full border-[1.5px] border-cyan-800 rounded-lg duration-75 duration-75
                        active:border-cyan-800 focus:border-cyan-800 focus:outline-offset-0 focus:shadow-none">
                    @foreach($toppings as $topping)
                        <option value="{{ $topping->id }}">{{ $topping->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-4">
                <x-label class="ml-4 mb-2 text-cyan-800 font-bold text-md" for="order[notify_type]" :value="__('Notify')" />

                <select name="order[notify_type]" id="order[notify_type]"
                        class="w-full border-[1.5px] border-cyan-800 rounded-lg duration-75 duration-75
                        active:border-cyan-800 focus:border-cyan-800 focus:outline-offset-0 focus:shadow-none">
                    @foreach(NotifyType::all()->toArray() as $value => $title)
                        <option value="{{ $value }}">{{ $title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="flex items-center justify-end mt-8 w-full">
                <x-button class="py-3 px-20 bg-cyan-800 rounded-lg">
                    {{ __('Order') }}
                </x-button>
            </div>
        </form>
        @if(count($orders) !== 0)
            <hr class="my-4">
            @if(Session::has('success'))
                <div class="border-[1.5px] border-emerald-500 bg-emerald-300 px-4 py-3 rounded-lg mb-4">
                    <h3 class="text-lg">{{ Session::get('success') }}</h3>
                    @if(Session::has('notify'))
                        <hr class="my-2 border-emerald-500">
                        <h3 class="text-lg">{{ Session::get('notify') }}</h3>
                    @endif
                </div>
            @endif
            <h1 class="text-4xl font-light text-cyan-800 uppercase">Orders</h1>
            <hr class="mt-4">
            <table class="text-left w-full">
                <tr class="text-cyan-800">
                    <th class="p-2 font-bold text-2xl text-center">#</th>
                    <th class="p-2 table-cell align-bottom">Pizza</th>
                    <th class="p-2 table-cell align-bottom">Status</th>
                    <th class="p-2 table-cell align-bottom">Delivery</th>
                </tr>
                @foreach($orders as $order)
                    <tr class="border-4 border-white bg-stone-200">
                        <td class="rounded-l-2xl py-2 px-4 w-[10%] border-r-4 font-bold bg-gray-300 border-white text-xl text-white">{{ $order->getKey() }}</td>
                        <td class="py-2 px-4 w-[33%] border-r-4 border-white">{{ $order->pizza->title }}</td>
                        <td class="py-2 px-4 w-[33%] border-r-4 border-white">{{ $order->status->title() }}</td>
                        <td class="rounded-r-2xl py-2 px-4 w-[29%] border-r-4 border-white">{{ $order->branch->delivery->title() }}</td>
                    </tr>
                @endforeach
            </table>
            @if($orders->hasPages())
                <hr class="my-4">
                {{ $orders->links() }}
            @endif
        @endif
    </x-card>
</x-guest-layout>
