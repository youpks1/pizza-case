@props(['active'])

@php
$classes = ($active ?? false)
            ? 'inline-flex items-center px-1 pt-1 border-b-2 border-indigo-400 text-sm font-medium leading-5 text-white focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out p-2 bg-cyan-800 border-tl-lg border-tr-lg'
            : 'inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out';
@endphp

{{--<a {{ $attributes->merge(['class' => $classes]) }}>--}}
<a {{ $attributes->merge(['class' => 'inline-flex items-center rounded-tr-lg rounded-tl-lg py-1 border-b-2 border-indigo-400 text-sm font-medium leading-5 text-white focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out bg-cyan-800 border-lt-lg border-rt-lg px-6']) }}>
    {{ $slot }}
</a>
