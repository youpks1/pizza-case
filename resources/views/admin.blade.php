<?php use App\Enums\Status; ?>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Admin') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if(count($orders) !== 0)
                        @if(Session::has('success'))
                            <div class="bg-green-200 px-4 py-3 rounded-md mb-4">
                                <h3 class="text-lg">{{ Session::get('success') }}</h3>
                            </div>
                        @endif
                        <h1 class="text-4xl font-light text-cyan-800 uppercase">Orders</h1>
                            <hr class="mt-4 mb-1">
                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <table class="text-left w-full">
                            <tr class="text-cyan-800">
                                <th class="p-2 font-bold text-2xl text-center">#</th>
                                <th class="p-2 table-cell align-bottom">Branch</th>
                                <th class="p-2 table-cell align-bottom">Pizza</th>
                                <th class="p-2 table-cell align-bottom" colspan="2">Status</th>
                            </tr>
                            @foreach($orders as $order)
                                <form method="POST" action="{{ route('order.update.status', ['order' => $order]) }}">
                                    @csrf
                                    <tr class="border-4 border-white bg-stone-200">
                                        <td class="rounded-l-3xl py-4 px-6 w-[5%] border-r-4 font-bold bg-gray-300 border-white text-xl text-white">{{ $order->getKey() }}</td>
                                        <td class="py-4 px-6 w-[20%] border-r-4 border-white">{{ $order->branch->title }}</td>
                                        <td class="py-4 px-6 w-[25%] border-r-4 border-white">{{ $order->pizza->title }}</td>
                                        <td class="py-4 px-6 w-[25%]">
                                            <select name="status" id="status"
                                                    class="w-full border-[1.5px] border-cyan-800 bg-stone-200 rounded-lg duration-75 duration-75
                                                    active:border-cyan-800 focus:border-cyan-800 focus:outline-offset-0 focus:shadow-none">
                                                @foreach(Status::all()->toArray() as $value => $title)
                                                    <option @if($order->status->value === $value) selected @endif value="{{ $value }}">{{ $title }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="rounded-r-3xl py-4 px-6 w-[25%]">
                                            <x-button class="ml-4 py-3 px-12 bg-cyan-800 rounded-lg">
                                                {{ __('Status update') }}
                                            </x-button>
                                        </td>
                                    </tr>
                                </form>
                            @endforeach
                        </table>
                        @if($orders->hasPages())
                            <hr class="my-4">
                            {{ $orders->links() }}
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
