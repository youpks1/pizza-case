<?php

namespace Database\Seeders;

use App\Enums\NotifyType;
use App\Enums\Status;
use App\Models\Branch;
use App\Models\Order;
use App\Models\Pizza;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
            ->count(20)
            ->state(new Sequence(fn ($sequence) => [
                'branch_id' => Branch::all()->random(),
                'pizza_id' => Pizza::all()->random(),
                'status' => Arr::random(Status::cases()),
                'notify_type' => Arr::random(NotifyType::cases()),
            ]))
            ->create();
    }
}
