<?php

namespace Database\Seeders;

use App\Enums\IngredientType;
use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingredient::factory()
            ->create([
                'title' => 'Classic',
                'type' => IngredientType::BOTTOM,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Cheesy Crust',
                'type' => IngredientType::BOTTOM,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Ultra Thin',
                'type' => IngredientType::BOTTOM,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Hawaii',
                'type' => IngredientType::TOPPING,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Hot & Spicy',
                'type' => IngredientType::TOPPING,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Bolognese',
                'type' => IngredientType::TOPPING,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Quattro Formaggi',
                'type' => IngredientType::TOPPING,
            ]);

        Ingredient::factory()
            ->create([
                'title' => 'Salami',
                'type' => IngredientType::TOPPING,
            ]);
    }
}
