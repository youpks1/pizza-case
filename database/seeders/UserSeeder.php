<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->password('password')
            ->create([
                'name' => 'Youp Koopmans',
                'email' => 'youp@test.com',
                'phone' => '+31 6 41616968',
            ]);

        User::factory()
            ->password('password')
            ->create([
                'name' => 'Admin',
                'email' => 'admin@test.com',
                'phone' => '+31 6 12345678',
            ]);
    }
}
