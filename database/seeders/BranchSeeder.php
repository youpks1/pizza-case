<?php

namespace Database\Seeders;

use App\Enums\Delivery;
use App\Models\Branch;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::factory()
            ->create([
                'title' => 'Dominos',
                'delivery' => Delivery::DELIVER,
            ]);

        Branch::factory()
            ->create([
                'title' => 'New York Pizza',
                'delivery' => Delivery::TAKE_AWAY,
            ]);
    }
}
