<?php

namespace Database\Seeders;

use App\Models\Pizza;
use Illuminate\Database\Seeder;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pizza::factory()
            ->attachBottom(0)
            ->attachTopping(0)
            ->create();

        Pizza::factory()
            ->attachBottom(1)
            ->attachTopping(1)
            ->create();

        Pizza::factory()
            ->attachBottom(0)
            ->attachTopping(1)
            ->create();

        Pizza::factory()
            ->attachBottom(1)
            ->attachTopping(0)
            ->create();
    }
}
