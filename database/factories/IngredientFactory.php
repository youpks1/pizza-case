<?php

namespace Database\Factories;

use App\Enums\IngredientType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class IngredientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => fake()->name,
            'type' => IngredientType::BOTTOM,
        ];
    }
}
