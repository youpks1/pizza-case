<?php

namespace Database\Factories;

use App\Enums\IngredientType;
use App\Models\Ingredient;
use App\Models\Pizza;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class PizzaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
        ];
    }

    public function attachBottom(int $index = 0): self
    {
        return $this->afterCreating(
            fn (Pizza $pizza) => $pizza
            ->bottom()
            ->attach(
                Ingredient::query()
                ->whereType(type: IngredientType::BOTTOM)
                ->get()[$index]
                ->getKey(),
            ),
        );
    }

    public function attachTopping(int $index = 0): self
    {
        return $this->afterCreating(
            fn (Pizza $pizza) => $pizza
            ->topping()
            ->attach(
                Ingredient::query()
                ->whereType(type: IngredientType::TOPPING)
                ->get()[$index]
                ->getKey(),
            ),
        );
    }
}
