<?php

namespace Database\Factories;

use App\Enums\Delivery;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class BranchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => fake()->name,
            'delivery' => Delivery::TAKE_AWAY,
        ];
    }
}
