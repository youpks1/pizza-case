<?php

namespace App\Enums\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

trait EnumMethods
{
    /**
     * Get all the cases with their title.
     */
    public static function all(): Collection
    {
        return collect(self::cases())
            ->mapWithKeys(fn (self $enum) => [
                $enum->value => $enum->title(),
            ])->sort();
    }

    /**
     * Get the translated name.
     */
    public function title(): string
    {
        if (Lang::has(Str::lower(self::translationNamePath()).'.'.$this->name)) {
            return __((Str::lower(self::translationNamePath()).'.'.$this->name));
        }

        return $this->name;
    }

    public static function migration(): array
    {
        return collect(self::cases())
            ->map(
                fn (self $enum): string => $enum->value,
            )->toArray();
    }
}
