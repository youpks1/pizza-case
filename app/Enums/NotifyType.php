<?php

declare(strict_types=1);

namespace App\Enums;

use App\Enums\Traits\EnumMethods;
use App\Enums\Contracts\HasTranslationFile;

enum NotifyType: string implements HasTranslationFile
{
    use EnumMethods;

    case EMAIL = 'email';
    case SMS = 'sms';

    public static function translationNamePath(): string
    {
        return 'notify-type';
    }
}
