<?php

declare(strict_types=1);

namespace App\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\Traits\EnumMethods;

enum IngredientType: string implements HasTranslationFile
{
    use EnumMethods;

    case BOTTOM = 'bottom';
    case TOPPING = 'topping';

    public static function translationNamePath(): string
    {
        return 'ingredient-type';
    }
}
