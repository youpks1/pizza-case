<?php

namespace App\Enums\Contracts;

interface HasTranslationFile
{
    public static function translationNamePath(): string;
}
