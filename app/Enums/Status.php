<?php

declare(strict_types=1);

namespace App\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\Traits\EnumMethods;

enum Status: string implements HasTranslationFile
{
    use EnumMethods;

    case ORDER_RECEIVED = 'order_received';
    case PIZZA_PREPARED = 'pizza_prepared';
    case IN_THE_OVEN = 'in_the_oven';
    case ON_THE_WAY = 'on_the_way';
    case DELIVERED = 'delivered';

    public static function translationNamePath(): string
    {
        return 'status';
    }
}
