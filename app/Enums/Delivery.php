<?php

declare(strict_types=1);

namespace App\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\Traits\EnumMethods;

enum Delivery: string implements HasTranslationFile
{
    use EnumMethods;

    case DELIVER = 'deliver';
    case TAKE_AWAY = 'take_away';

    public static function translationNamePath(): string
    {
        return 'delivery';
    }
}
