<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\DataTransferObjects\Auth\RegisterDto;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\Auth\RegisterRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class RegisteredUserController extends Controller
{
    public function __construct(private readonly RegisterRepository $registerRepository)
    {
    }

    /**
     * Display the registration view.
     *
     * @return View
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param RegisterRequest $request
     * @return RedirectResponse
     * @throws UnknownProperties
     */
    public function store(RegisterRequest $request): RedirectResponse
    {
        $user = $this->registerRepository
            ->create(
                new RegisterDto($request->register()),
            );

        Auth::login($user);

        return redirect()->route('admin');
    }
}
