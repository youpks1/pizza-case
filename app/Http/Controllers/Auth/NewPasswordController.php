<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\DataTransferObjects\Auth\PasswordResetDto;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Auth\PasswordResetRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\View\View;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class NewPasswordController extends Controller
{
    public function __construct(private readonly PasswordResetRepository $passwordResetRepository)
    {
    }

    /**
     * Display the password reset view.
     *
     * @param Request $request
     * @return View
     */
    public function create(Request $request): View
    {
        return view('auth.reset-password', [
            'request' => $request,
        ]);
    }

    /**
     * Handle an incoming new password request.
     *
     * @param PasswordResetRequest $request
     * @return RedirectResponse
     *
     * @throws UnknownProperties
     */
    public function store(PasswordResetRequest $request): RedirectResponse
    {
        $status = $this->passwordResetRepository->reset(new PasswordResetDto($request->validated()));

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('auth.login')->with('status', __($status))
                    : back()->withInput($request->only('email'))
                            ->withErrors(['email' => __($status)]);
    }
}
