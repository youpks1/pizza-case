<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DataTransferObjects\OrderDto;
use App\DataTransferObjects\OrderStatusDto;
use App\DataTransferObjects\PizzaDto;
use App\Http\Requests\StatusUpdateOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Repositories\PizzaRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class OrderController extends Controller
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly PizzaRepository $pizzaRepository,
    ) {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrderRequest $request
     * @return RedirectResponse
     * @throws UnknownProperties
     */
    public function store(StoreOrderRequest $request): RedirectResponse
    {
        $order = $this->orderRepository->create(
            new OrderDto(array_merge($request->validated()['order'], [
                'pizza_id' => $this->pizzaRepository->firstOrCreate(
                    new PizzaDto($request->validated()['pizza']),
                )->getKey(),
            ])),
        );

        Session::flash('success', "Order #{$order->getKey()} created successfully");

        return back();
    }

    /**
     * Update the status of an order.
     *
     * @param StatusUpdateOrderRequest $request
     * @param Order $order
     * @return RedirectResponse
     * @throws UnknownProperties
     */
    public function statusUpdate(StatusUpdateOrderRequest $request, Order $order): RedirectResponse
    {
        $this->orderRepository->statusUpdate(
            $order,
            new OrderStatusDto($request->validated()),
        );

        Session::flash('success', "Order #{$order->getKey()} status updated successfully");

        return back();
    }
}
