<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\IngredientType;
use App\Models\Branch;
use App\Models\Ingredient;
use App\Models\Order;
use Illuminate\View\View;

class HomePageController extends Controller
{
    /**
     * Display the home view.
     *
     * @return View
     */
    public function __invoke(): View
    {
        $ingredients = Ingredient::all();

        return view('home', [
            'branches' => Branch::all(),
            'toppings' => $ingredients
                ->filter(static fn (Ingredient $ingredient) => $ingredient->type === IngredientType::TOPPING),
            'bottoms' => $ingredients
                ->filter(static fn (Ingredient $ingredient) => $ingredient->type === IngredientType::BOTTOM),
            'orders' => Order::query()
                    ->with(['branch', 'pizza'])
                    ->orderBy('id', 'DESC')
                    ->paginate(5),
        ]);
    }
}
