<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\View\View;

class AdminPageController extends Controller
{
    /**
     * Display the admin view.
     *
     * @return View
     */
    public function __invoke(): View
    {
        return view('admin', [
            'orders' => Order::query()
                ->with(['branch', 'pizza'])
                ->orderBy('id', 'DESC')
                ->paginate(10),
        ]);
    }
}
