<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\NotifyType;
use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'order.branch_id' => ['required', 'integer'],
            'order.status' => ['required', new Rules\Enum(Status::class)],
            'order.notify_type' => ['required', new Rules\Enum(NotifyType::class)],
            'pizza.bottom' => ['required', 'integer'],
            'pizza.topping' => ['required', 'integer'],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'order' => array_merge($this->input('order'), ['status' => Status::ORDER_RECEIVED->value]),
        ]);
    }
}
