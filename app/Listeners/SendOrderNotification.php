<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Enums\NotifyType;
use App\Events\OrderCreated;
use App\Notifications\OrderCreatedNotification;
use Illuminate\Support\Facades\Session;

class SendOrderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderCreated $event
     * @return void
     */
    public function handle(OrderCreated $event): void
    {
        if (auth()->check()) {
            match ($event->order->notify_type) {
                NotifyType::EMAIL => auth()->user()?->notify(new OrderCreatedNotification($event->order))?->via('mail'),
                NotifyType::SMS => auth()->user()?->notify(new OrderCreatedNotification($event->order))?->via('vonage'),
                default => $event->order,
            };
            Session::flash('notify', "{$event->order->notify_type->title()} send!");
        } else {
            Session::flash('notify', 'Log in for notifications!');
        }
    }
}
