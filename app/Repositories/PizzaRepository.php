<?php

declare(strict_types=1);

namespace App\Repositories;

use App\DataTransferObjects\PizzaDto;
use App\Models\Pizza;

class PizzaRepository
{
    public function firstOrCreate(PizzaDto $inputs): Pizza
    {
        return Pizza::query()->firstOrNewModel($inputs);
    }
}
