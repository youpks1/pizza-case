<?php

declare(strict_types=1);

namespace App\Repositories;

use App\DataTransferObjects\OrderDto;
use App\DataTransferObjects\OrderStatusDto;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    /**
     * @param OrderDto $inputs
     * @return Order
     */
    public function create(OrderDto $inputs): Order
    {
        return DB::transaction(static function () use ($inputs) {
            return Order::query()->create($inputs->toArray());
        });
    }

    /**
     * @param Order $order
     * @param OrderStatusDto $status
     * @return void
     */
    public function statusUpdate(Order $order, OrderStatusDto $status): void
    {
        DB::transaction(static function () use ($order, $status) {
            $order->update($status->toArray());
        });
    }
}
