<?php

declare(strict_types=1);

namespace App\Repositories\Auth;

use App\DataTransferObjects\Auth\PasswordResetDto;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class PasswordResetRepository
{
    /**
     * @param PasswordResetDto $inputs
     * @return string
     */
    public function reset(PasswordResetDto $inputs): string
    {
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        return Password::reset(
            $inputs->toArray(),
            static function ($user) use ($inputs) {
                $user->forceFill([
                    'password' => Hash::make($inputs->password),
                    'remember_token' => Str::random(60),
                ])->save();

                event(new PasswordReset($user));
            },
        );
    }
}
