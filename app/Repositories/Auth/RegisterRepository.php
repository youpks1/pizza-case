<?php

declare(strict_types=1);

namespace App\Repositories\Auth;

use App\DataTransferObjects\Auth\RegisterDto;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RegisterRepository
{
    /**
     * @param RegisterDto $inputs
     * @return User
     */
    public function create(RegisterDto $inputs): User
    {
        return DB::transaction(static function () use ($inputs) {
            return User::query()->create($inputs->toArray());
        });
    }
}
