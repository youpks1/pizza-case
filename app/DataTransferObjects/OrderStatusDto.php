<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

use App\DataTransferObjects\Casters\StatusEnumCaster;
use App\Enums\Status;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;

class OrderStatusDto extends DataTransferObject
{
    #[CastWith(StatusEnumCaster::class)]
    public Status $status;
}
