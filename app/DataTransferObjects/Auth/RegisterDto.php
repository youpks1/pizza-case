<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Auth;

use Spatie\DataTransferObject\DataTransferObject;

class RegisterDto extends DataTransferObject
{
    public string $name;
    public string $email;
    public string $password;
    public string $phone;
}
