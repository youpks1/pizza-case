<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Auth;

use Spatie\DataTransferObject\DataTransferObject;

class PasswordResetDto extends DataTransferObject
{
    public string $token;
    public string $email;
    public string $password;
    public string $password_confirmation;
    public string $phone;
}
