<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Casters;

use App\Enums\Status;
use Spatie\DataTransferObject\Caster;

class StatusEnumCaster implements Caster
{
    public function cast(mixed $value): Status
    {
        return Status::from($value);
    }
}
