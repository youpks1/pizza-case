<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Casters;

use App\Enums\NotifyType;
use Spatie\DataTransferObject\Caster;

class NotifyTypeEnumCaster implements Caster
{
    public function cast(mixed $value): NotifyType
    {
        return NotifyType::from($value);
    }
}
