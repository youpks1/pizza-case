<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

use App\DataTransferObjects\Casters\NotifyTypeEnumCaster;
use App\DataTransferObjects\Casters\StatusEnumCaster;
use App\Enums\NotifyType;
use App\Enums\Status;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;

class OrderDto extends DataTransferObject
{
    public int $branch_id;
    public int $pizza_id;

    #[CastWith(StatusEnumCaster::class)]
    public Status $status;

    #[CastWith(NotifyTypeEnumCaster::class)]
    public NotifyType $notify_type;
}
