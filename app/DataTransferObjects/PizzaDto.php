<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class PizzaDto extends DataTransferObject
{
    public int $bottom;
    public int $topping;
}
