<?php

declare(strict_types=1);

namespace App\QueryBuilders;

use App\Enums\IngredientType;
use Illuminate\Database\Eloquent\Builder;

class IngredientQueryBuilder extends Builder
{
    public function whereId(int $id): static
    {
        return $this->where('ingredients.id', $id);
    }

    public function whereType(IngredientType $type): static
    {
        return $this->where('ingredients.type', $type->value);
    }
}
