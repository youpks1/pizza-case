<?php

declare(strict_types=1);

namespace App\QueryBuilders;

use App\DataTransferObjects\PizzaDto;
use App\Enums\IngredientType;
use App\Models\Pizza;
use Illuminate\Database\Eloquent\Builder;
use Closure;
use Illuminate\Database\Eloquent\Model;

class PizzaQueryBuilder extends Builder
{
    public function whereHasIngredient(IngredientType $ingredient, int $ingredientId = null, Closure $callback = null): static
    {
        return $this->whereHas($ingredient->value, fn (IngredientQueryBuilder $query) => $query
            ->when(
                $ingredientId,
                fn (IngredientQueryBuilder $query) => $query->whereId(id: $ingredientId),
                $callback,
            ), );
    }

    public function firstOrNewModel(PizzaDto $inputs): Pizza
    {
        return $this
            ->whereHasIngredient(
                ingredient: IngredientType::BOTTOM,
                ingredientId: $inputs->bottom,
            )->whereHasIngredient(
                ingredient:IngredientType::TOPPING,
                ingredientId: $inputs->topping,
            )->firstOr(function () use ($inputs): Pizza|Model {
                $newPizza = Pizza::query()->create();
                $newPizza->bottom()->attach($inputs->bottom);
                $newPizza->topping()->attach($inputs->topping);

                return $newPizza;
            });
    }
}
