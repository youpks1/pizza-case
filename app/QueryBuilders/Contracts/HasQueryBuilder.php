<?php

namespace App\QueryBuilders\Contracts;

interface HasQueryBuilder
{
    public function newEloquentBuilder($query);
}
