<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // When behind a loadbalancer, Laravel thinks it's being run as http
        if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && Str::lower($_SERVER['HTTP_X_FORWARDED_PROTO']) === 'https') {
            URL::forceScheme('https');
        }

        // Disable lazy loading entirely (exception will be thrown), only in non-production environments.
        Model::preventLazyLoading(! $this->app->isProduction());
    }
}
