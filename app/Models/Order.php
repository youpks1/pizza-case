<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\NotifyType;
use App\Enums\Status;
use App\Events\OrderCreated;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use App\QueryBuilders\OrderQueryBuilder;
use Carbon\Carbon;
use Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Order.
 *
 * @property int $id
 * @property int $branch_id
 * @property int $pizza_id
 * @property Status $status
 * @property NotifyType $notify_type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Branch $branch
 * @property-read Pizza $pizza
 * @method static OrderFactory factory(...$parameters)
 * @method static OrderQueryBuilder query()
 */
class Order extends Model implements HasQueryBuilder
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'status' => Status::class,
        'notify_type' => NotifyType::class,
    ];

    protected $dispatchesEvents = [
        'created' => OrderCreated::class,
    ];

    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }

    public function pizza(): BelongsTo
    {
        return $this->belongsTo(Pizza::class);
    }

    public function newEloquentBuilder($query): OrderQueryBuilder
    {
        return new OrderQueryBuilder($query);
    }
}
