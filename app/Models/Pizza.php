<?php

declare(strict_types=1);

namespace App\Models;

use App\DataTransferObjects\PizzaDto;
use App\Enums\IngredientType;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use App\QueryBuilders\PizzaQueryBuilder;
use Carbon\Carbon;
use Database\Factories\PizzaFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Pizza.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Ingredient[] $bottom
 * @property-read Ingredient[] $topping
 * @property-read Order[] $orders
 * @property-read string $title
 * @method static PizzaFactory factory(...$parameters)
 * @method static PizzaQueryBuilder query()
 * @method static PizzaQueryBuilder whereHasIngredient(IngredientType $ingredient, ?int $ingredientId = null, ?\Closure $callback = null)
 * @method static PizzaQueryBuilder firstOrNewModel(PizzaDto $inputs)
 */
class Pizza extends Model implements HasQueryBuilder
{
    use HasFactory;

    protected $guarded = [];

    public function topping(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class)
            ->where('type', IngredientType::TOPPING);
    }

    public function bottom(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class)
            ->where('type', IngredientType::BOTTOM);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function title(): Attribute
    {
        return new Attribute(
            get: fn (): string => $this->bottom()->first()->title.' - '.$this->topping()->first()->title,
        );
    }

    public function newEloquentBuilder($query): PizzaQueryBuilder
    {
        return new PizzaQueryBuilder($query);
    }
}
