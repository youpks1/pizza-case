<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\IngredientType;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use App\QueryBuilders\IngredientQueryBuilder;
use Carbon\Carbon;
use Database\Factories\IngredientFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Ingredient.
 *
 * @property int $id
 * @property string $title
 * @property IngredientType $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Pizza[] $pizzas
 * @method static IngredientFactory factory(...$parameters)
 * @method static IngredientQueryBuilder query()
 * @method static IngredientQueryBuilder whereId(int $id)
 * @method static IngredientQueryBuilder whereType(IngredientType $type)
 */
class Ingredient extends Model implements HasQueryBuilder
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'type' => IngredientType::class,
    ];

    public function pizzas(): BelongsToMany
    {
        return $this->belongsToMany(Pizza::class);
    }

    public function newEloquentBuilder($query): IngredientQueryBuilder
    {
        return new IngredientQueryBuilder($query);
    }
}
