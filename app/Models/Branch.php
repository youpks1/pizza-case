<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\Delivery;
use App\QueryBuilders\BranchQueryBuilder;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Carbon\Carbon;
use Database\Factories\BranchFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Branch.
 *
 * @property int $id
 * @property string $title
 * @property Delivery $delivery
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Order[] $orders
 * @method static BranchFactory factory(...$parameters)
 * @method static BranchQueryBuilder query()
 */
class Branch extends Model implements HasQueryBuilder
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'delivery' => Delivery::class,
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function newEloquentBuilder($query): BranchQueryBuilder
    {
        return new BranchQueryBuilder($query);
    }
}
