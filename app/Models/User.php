<?php

declare(strict_types=1);

namespace App\Models;

use App\QueryBuilders\Contracts\HasQueryBuilder;
use App\QueryBuilders\UserQueryBuilder;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User.
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string|null $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static UserFactory factory(...$parameters)
 * @method static UserQueryBuilder query()
 */
class User extends Authenticatable implements HasQueryBuilder
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function newEloquentBuilder($query): UserQueryBuilder
    {
        return new UserQueryBuilder($query);
    }

    /**
     * Route notifications for the Vonage channel.
     *
     * @param Notification $notification
     * @return string
     */
    public function routeNotificationForVonage(Notification $notification): string
    {
        return $this->phone;
    }
}
