<?php

declare(strict_types=1);

return [
    'DELIVER' => 'Deliver',
    'TAKE_AWAY' => 'Take away',
];
