<?php

declare(strict_types=1);

return [
    'ORDER_RECEIVED' => 'Order received',
    'PIZZA_PREPARED' => 'Pizza prepared',
    'IN_THE_OVEN' => 'In the oven',
    'ON_THE_WAY' => 'On the way',
    'DELIVERED' => 'Delivered',
];
