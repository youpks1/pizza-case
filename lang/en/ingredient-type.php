<?php

declare(strict_types=1);

return [
    'BOTTOM' => 'Bottom',
    'TOPPING' => 'Topping',
];
