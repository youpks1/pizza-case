<?php

declare(strict_types=1);

return [
    'EMAIL' => 'Email',
    'SMS' => 'SMS',
];
