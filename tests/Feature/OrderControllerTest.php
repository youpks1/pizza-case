<?php

namespace Tests\Feature;

use App\Enums\IngredientType;
use App\Enums\NotifyType;
use App\Enums\Status;
use App\Models\Branch;
use App\Models\Ingredient;
use App\Models\Order;
use App\Models\Pizza;
use App\Models\User;
use App\Notifications\OrderCreatedNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_new_order_can_be_created(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        auth()->login($user);

        $branch = Branch::factory()->create([
            'title' => 'Branch',
        ]);

        $pizza = Pizza::factory()
            ->has(Ingredient::factory([
                'title' => 'Bottom',
                'type' => IngredientType::BOTTOM,
            ]), 'bottom')
            ->has(
                Ingredient::factory([
                    'title' => 'Topping',
                    'type' => IngredientType::TOPPING,
                ]),
                'topping',
            )->create();

        $bottom = Ingredient::query()
            ->where('title', 'Bottom')
            ->where('type', IngredientType::BOTTOM)
            ->first();

        $topping = Ingredient::query()
            ->where('title', 'Topping')
            ->where('type', IngredientType::TOPPING)
            ->first();

        $response = $this->post('/order/store', [
            'order' => [
                'branch_id' => $branch->getKey(),
                'notify_type' => NotifyType::EMAIL->value,
            ],
            'pizza' => [
                'bottom' => $bottom->getKey(),
                'topping' => $topping->getKey(),
            ],
        ]);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $this->assertDatabaseCount(Order::class, 1);
        $this->assertDatabaseHas(Order::class, [
            'branch_id' => $branch->getKey(),
            'pizza_id' => $pizza->getKey(),
            'status' => Status::ORDER_RECEIVED,
            'notify_type' => NotifyType::EMAIL,
        ]);

        Notification::assertSentTo(
            $user,
            static function (OrderCreatedNotification $notification) {
                return $notification->order->getKey() === 1;
            },
        );
    }

    public function test_order_status_can_be_updated(): void
    {
        $user = User::factory()->create();

        $order = Order::factory()->create([
            'branch_id' => Branch::factory()->create()->getKey(),
            'pizza_id' => Pizza::factory()
                ->has(Ingredient::factory([
                    'title' => 'Bottom',
                    'type' => IngredientType::BOTTOM,
                ]), 'bottom')
                ->has(
                    Ingredient::factory([
                        'title' => 'Topping',
                        'type' => IngredientType::TOPPING,
                    ]),
                    'topping',
                )->create()
                ->getKey(),
            'status' => Status::ORDER_RECEIVED->value,
            'notify_type' => NotifyType::EMAIL->value,
        ]);

        $response = $this->actingAs($user)->post('/admin/order/status/update/'.$order->getKey(), [
            'status' => Status::DELIVERED->value,
        ]);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $this->assertDatabaseHas(Order::class, [
            'branch_id' => $order->branch->getKey(),
            'pizza_id' => $order->pizza->getKey(),
            'status' => Status::DELIVERED,
            'notify_type' => NotifyType::EMAIL,
        ]);
    }
}
