<?php

namespace Tests\Unit\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\Status;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;

class StatusTest extends TestCase
{
    public function test_all_cases_have_their_translation(): void
    {
        foreach (Status::cases() as $case) {
            self::assertTrue(Lang::has(Status::translationNamePath().'.'.$case->name));
        }
    }

    public function test_it_has_a_translation_name_path(): void
    {
        self::assertNotNull(Status::translationNamePath());
        self::assertTrue(is_a(Status::class, HasTranslationFile::class, true));
    }
}
