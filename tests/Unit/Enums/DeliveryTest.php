<?php

namespace Tests\Unit\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\Delivery;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;

class DeliveryTest extends TestCase
{
    public function test_all_cases_have_their_translation(): void
    {
        foreach (Delivery::cases() as $case) {
            self::assertTrue(Lang::has(Delivery::translationNamePath().'.'.$case->name));
        }
    }

    public function test_it_has_a_translation_name_path(): void
    {
        self::assertNotNull(Delivery::translationNamePath());
        self::assertTrue(is_a(Delivery::class, HasTranslationFile::class, true));
    }
}
