<?php

namespace Tests\Unit\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\IngredientType;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;

class IngredientTypeTest extends TestCase
{
    public function test_all_cases_have_their_translation(): void
    {
        foreach (IngredientType::cases() as $case) {
            self::assertTrue(Lang::has(IngredientType::translationNamePath().'.'.$case->name));
        }
    }

    public function test_it_has_a_translation_name_path(): void
    {
        self::assertNotNull(IngredientType::translationNamePath());
        self::assertTrue(is_a(IngredientType::class, HasTranslationFile::class, true));
    }
}
