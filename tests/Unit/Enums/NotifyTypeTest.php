<?php

namespace Tests\Unit\Enums;

use App\Enums\Contracts\HasTranslationFile;
use App\Enums\NotifyType;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;

class NotifyTypeTest extends TestCase
{
    public function test_all_cases_have_their_translation(): void
    {
        foreach (NotifyType::cases() as $case) {
            self::assertTrue(Lang::has(NotifyType::translationNamePath().'.'.$case->name));
        }
    }

    public function test_it_has_a_translation_name_path(): void
    {
        self::assertNotNull(NotifyType::translationNamePath());
        self::assertTrue(is_a(NotifyType::class, HasTranslationFile::class, true));
    }
}
