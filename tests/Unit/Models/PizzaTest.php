<?php

namespace Tests\Unit\Models;

use App\Enums\IngredientType;
use App\Models\Ingredient;
use App\Models\Pizza;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PizzaTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pizza = Pizza::factory()
            ->has(Ingredient::factory([
                'title' => 'Bottom',
                'type' => IngredientType::BOTTOM,
            ]), 'bottom')
            ->has(
                Ingredient::factory([
                    'title' => 'Topping',
                    'type' => IngredientType::TOPPING,
                ]),
                'topping',
            )->create();
    }

    public function test_title_attribute_is_bottom_and_topping_title(): void
    {
        self::assertSame('Bottom - Topping', $this->pizza->title);
    }

    public function test_the_model_has_its_own_query_builder(): void
    {
        self::assertTrue(is_a(Pizza::class, HasQueryBuilder::class, true));
    }

    public function test_the_bottom_relation_returns_the_bottom_type_ingredient(): void
    {
        self::assertInstanceOf(Ingredient::class, $this->pizza->bottom()->first());
        self::assertSame(IngredientType::BOTTOM, $this->pizza->bottom()->first()->type);
    }
}
