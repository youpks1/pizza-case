<?php

namespace Tests\Unit\Models;

use App\Enums\Delivery;
use App\Models\Branch;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BranchTest extends TestCase
{
    use RefreshDatabase;

    public function test_delivery_attribute_is_casted_to_the_delivery_enum(): void
    {
        $branch = Branch::factory()
            ->create([
                'delivery' => Delivery::TAKE_AWAY,
            ]);

        self::assertSame(Delivery::TAKE_AWAY, $branch->delivery);
    }

    public function test_the_model_has_its_own_query_builder(): void
    {
        self::assertTrue(is_a(Branch::class, HasQueryBuilder::class, true));
    }
}
