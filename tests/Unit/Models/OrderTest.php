<?php

namespace Tests\Unit\Models;

use App\Enums\IngredientType;
use App\Enums\NotifyType;
use App\Enums\Status;
use App\Models\Branch;
use App\Models\Ingredient;
use App\Models\Order;
use App\Models\Pizza;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->order = Order::factory()->create([
            'branch_id' => Branch::factory()->create()->getKey(),
            'pizza_id' => Pizza::factory()
                ->has(Ingredient::factory([
                    'title' => 'Bottom',
                    'type' => IngredientType::BOTTOM,
                ]), 'bottom')
                ->has(
                    Ingredient::factory([
                        'title' => 'Topping',
                        'type' => IngredientType::TOPPING,
                    ]),
                    'topping',
                )->create()
                ->getKey(),
            'status' => Status::ORDER_RECEIVED,
            'notify_type' => NotifyType::EMAIL,
        ]);
    }

    public function test_status_attribute_is_casted_to_the_status_enum(): void
    {
        self::assertSame(Status::ORDER_RECEIVED, $this->order->status);
    }

    public function test_notify_type_attribute_is_casted_to_the_notify_type_enum(): void
    {
        self::assertSame(NotifyType::EMAIL, $this->order->notify_type);
    }

    public function test_the_model_has_its_own_query_builder(): void
    {
        self::assertTrue(is_a(Order::class, HasQueryBuilder::class, true));
    }
}
