<?php

namespace Tests\Unit\Models;

use App\Models\User;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_the_model_has_its_own_query_builder(): void
    {
        self::assertTrue(is_a(User::class, HasQueryBuilder::class, true));
    }
}
