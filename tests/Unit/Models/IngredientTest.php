<?php

namespace Tests\Unit\Models;

use App\Enums\IngredientType;
use App\Models\Ingredient;
use App\QueryBuilders\Contracts\HasQueryBuilder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IngredientTest extends TestCase
{
    use RefreshDatabase;

    public function test_type_attribute_is_casted_to_the_ingredient_type_enum(): void
    {
        $ingredient = Ingredient::factory()
            ->create([
                'type' => IngredientType::TOPPING,
            ]);

        self::assertSame(IngredientType::TOPPING, $ingredient->type);
    }

    public function test_the_model_has_its_own_query_builder(): void
    {
        self::assertTrue(is_a(Ingredient::class, HasQueryBuilder::class, true));
    }
}
